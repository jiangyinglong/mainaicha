'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/sinadata', controller.user.tool);

  router.get('/test', controller.user.test);

  router.get('/user2test', controller.user.tool);

};
